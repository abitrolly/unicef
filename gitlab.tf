provider "gitlab" {
}

resource "gitlab_project" "default" {
  name             = "unicef"
  description      = "repository for Open Source Software Mentor job - http://jobs.unicef.org/cw/en-us/job/532955"
  default_branch   = "master"
  visibility_level = "public"
}
