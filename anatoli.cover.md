---
title: Cover Letter for UNICEF
author: Anatoli Babenia 
date: Jul 16 2020
geometry: top=1.8cm,bottom=1.8cm
---

My name is Anatoli Babenia, but my nickname is different. During my
15 year journey in Open Source I had few nicknames. It started as
a community movement. The people I didn't know, gave me commit access
to a public project, and that made me feel a part of something big. I
still remember this feeling. It was about trusting people and letting
others make mistakes. Version control systems made that possible
by allowing to revert things back. The project, TikiWiki had a few
simple guidelines that I still use to teach people. I liked that, but
even more inspiring were development guidelines for PHP language the
TikiWiki was written in, which contained a statement to increase the
presence of good will on the planet Earth. That was a great feeling,
and a lot of fun too. Things have changed since then. Now there is a
new generation. A whole startup scene appeared, some new people are
forced into Open Source rather than choose this way voluntary. Things
may need a lead.

In 2020 a lot of companies are trying to grasp what Open Source is.
I've consulted lawyers from big companies about licensing, so that
they could draft an effective policy, and small software companies
trying to resist the disruption of their business models. I've seen
projects financed by The EU struggle to become open. I've seen grants
from UNDP producing apps for developing countries in Africa, which
stopped working a few months after the grant ended, because there
were no sustainable model to support them. I helped journalists
maintain such projects, because the projects brought value to people.
I know the pressure of trying to keep up a full time job and helping
Open Source. There are still a lot of things to try, and I am glad
that UNICEF sees Open Source as a way to improve the future for
children.

In UNICEF mission I would like children to be co-creators of the
products they use. Not giving them the fish, but teaching them to
create and maintain the fishing rods. The tools that help them to
understand The world, the Economics, to visualise things and improve
on the work of each other though different gameplays. The scope of
my job may be a pure evaluation of technologies and projects, but I
would like to take this opportunity to see if we could build
something better, the Open Source way.

The code lasts only while it is in the head of its maintainers, and
to make a successful, long term impact, we need to make sure that
with the future ecosystems we develop, we are nurturing their
maintainers too.

---

This PDF was generated from scratch purely with Open Source
Software - Linux + vim + bash + pandoc.

https://gitlab.com/abitrolly/unicef
