#!/bin/sh

export SOURCE_DATE_EPOCH=2461551217

pandoc --from gfm \
	--variable urlcolor=green \
	-V mainfont="DejaVu Sans Mono" \
	-V geometry:a5paper,landscape \
	--pdf-engine xelatex \
	--include-in-header anatoli.head.tex \
	anatoli.cv.md -o anatoli.cv.pdf

pandoc --from markdown_github+yaml_metadata_block \
	--variable urlcolor=green \
	-V mainfont="DejaVu Sans Mono" \
	--pdf-engine xelatex \
	--include-in-header anatoli.head.tex \
	anatoli.cover.md -o anatoli.cover.pdf
